/*
	1. Create a function which is able to prompt the user to provide their fullname, age, and location.
		-use prompt() and store the returned value into a function scoped variable within the function.
		-display the user's inputs in messages in the console.
		-invoke the function to display your information in the console.
		-follow the naming conventions for functions.
*/

	
	//first function here:

    let userInfo = function(){
        let fullName = prompt("Enter Your Full Name :");
        let age = prompt("Enter Your Age : ");
        let location = prompt("Enter Your Location : ");

        console.log("The User's inputs are :" + fullName + "," +age + "," + location);
    }

    userInfo();

/*
	2. Create a function which is able to print/display your top 5 favorite bands/musical artists.
		-invoke the function to display your information in the console.
		-follow the naming conventions for functions.
	
*/

	// second function here:
    function favoriteBands(){
        console.log("Paramore");
        console.log("My Chemical Romance");
        console.log("Fall Out Boys");
        console.log("Red Jumpsuit Apparatus");
        console.log("Linkin Park");
    }
    favoriteBands();

/*
	3. Create a function which is able to print/display your top 5 favorite movies of all time and show Rotten Tomatoes rating.
		-Look up the Rotten Tomatoes rating of your favorite movies and display it along with the title of your favorite movie.
		-invoke the function to display your information in the console.
		-follow the naming conventions for functions.

*/
	
	//third function here:
    function favoriteMovies(){
        console.log("1. M3GAN (2022)\n Rotten Tomatoes Rating : 95%");
        console.log("2. The Menu (2022)\n Rotten Tomatoes Rating : 88%");
        console.log("3. Puss in Boots: The Last Wish (2022) \n Rotten Tomatoes Rating : 95%");
        console.log("4. The Pale Blue Eye (2022) \n Rotten Tomatoes Rating : 63%");
        console.log("5. Avatar: The Way of Water (2022) \n Rotten Tomatoes Rating : 77%");
    }
    favoriteMovies();

/*
	4. Debugging Practice - Debug the following codes and functions to avoid errors.
		-check the variable names
		-check the variable scope
		-check function invocation/declaration
		-comment out unusable codes.
*/

// printUsers();
let printFriends= function printUsers(){
	alert("Hi! Please add the names of your friends.");
	let friend1 = prompt("Enter your first friend's name:"); 
	let friend2 = prompt("Enter your second friend's name:"); 
	let friend3 = prompt("Enter your third friend's name:");

	console.log("You are friends with:")
	console.log(friend1); 
	console.log(friend2); 
	console.log(friend3); 
};
printFriends();

// console.logfriend1();
//console.log(friend2);